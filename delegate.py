from PySide2 import QtCore, QtWidgets


HEIGHT = 35
MIN_VALUE = 60


class Delegate(QtWidgets.QStyledItemDelegate):

    def __init__(self, table_size, column_count, parent=None):
        QtWidgets.QStyledItemDelegate.__init__(self, parent)

        self.table_size = table_size
        self.column_count = column_count

    def paint(self, painter, option, index):
        cell = index.data(QtCore.Qt.UserRole)

        cell_text = cell.short_name if index.column() > 2 else cell.value

        painter.drawText(
            option.rect,
            QtCore.Qt.AlignCenter,
            cell_text
        )

    def sizeHint(self, option, index):
        return QtCore.QSize(MIN_VALUE, HEIGHT)

