from PySide2 import QtWidgets
from delegate import Delegate


class MainWindow(QtWidgets.QWidget):

    def __init__(self, application, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.application = application
        self._init_ui()
        self._connect_ui()

    def _init_ui(self):
        self.setWindowTitle('Texture Manager')
        self.resize(1280, 720)

        self.main_layout = QtWidgets.QGridLayout()
        self.setLayout(self.main_layout)

        self.label_folder_path = QtWidgets.QLabel('Folder path: ')
        self.main_layout.addWidget(self.label_folder_path, 0, 0, 1, 1)

        folder_path = self.application.model_info().folder_path
        self.line_edit_folder_path = QtWidgets.QLineEdit(folder_path)
        self.main_layout.addWidget(self.line_edit_folder_path, 0, 1, 1, 5)

        self.table_view = QtWidgets.QTableView()
        self.main_layout.addWidget(self.table_view, 1, 0, 6, 6)
        self.table_view.setModel(self.application.qmodel)
        self.table_view.setItemDelegate(
            Delegate(
                table_size=self.table_view.size().width(),
                column_count=self._size_cells()
            )
        )
        self.table_view.verticalHeader().hide()
        self.table_view.resizeColumnsToContents()
        self.table_view.resizeRowsToContents()
        for column_ in range(self.application.model_info().column_count):
            if column_ in [1, 2]:
                continue
            self.table_view.horizontalHeader().setSectionResizeMode(column_, QtWidgets.QHeaderView.Stretch)

        self.button_save = QtWidgets.QPushButton('SAVE')
        self.main_layout.addWidget(self.button_save, 8, 5, 1, 1)

    def _connect_ui(self):
        self.button_save.clicked.connect(self._save)
        self.line_edit_folder_path.editingFinished.connect(self._edit_folder_path)

    def _size_cells(self):
        return self.application.model_info().column_count - 2

    def _save(self):
        self.application.save()

    def _edit_folder_path(self):
        self.application.set_folder_path(self.line_edit_folder_path.text())
