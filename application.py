
import sys
from PySide2 import QtWidgets
from texture_manager_model import TextureManagerModel
from main_window import MainWindow
from table_view_model import TableViewModel
from data import dataManager


class Application:

    def __init__(self):
        self.data = dataManager()
        self.texture_manager_model = TextureManagerModel(self.data)

        self.app = QtWidgets.QApplication(sys.argv)
        self.qmodel = TableViewModel(self)
        self.view = MainWindow(self)

    def model_info(self):
        """ Returns a set of info from the TableModelInfo class' attributes. These info are : the row count,
        the column count, and the column titles.

        NOTE FOR ANTHONY : To access the info, use TableModelInfo.rows_count / TableModelInfo.column_titles

        :return: a set of info concerning the table model
        :rtype instance of class TableModelInfo"""

        # TODO: for now the column titles are set as a TableModel's attribute. Consider making these user-settable via
        # a .json storage ?

        return self.texture_manager_model.info()

    def cell(self, row, column):
        return self.texture_manager_model.data[row][column]

    def update_cell(self, row, column, value):
        success = self.texture_manager_model.update_cell(row, column, value=value)
        self.texture_manager_model.update_row(row)
        return success

    def save(self):
        """ Save the data gathered in the array inside as a json file """
        self.data.save_data(self.texture_manager_model.data)
        pass

    def set_folder_path(self, path):
        """ Sets the folder path to use for generating texture paths

        :param path: New path to the project directory
        :type path: str
        """
        print(path)
        self.data.set_folder_path(path)
        self.texture_manager_model.folder_path = path
        self.texture_manager_model.build()


    def run(self):
        self.view.show()
        return self.app.exec_()
