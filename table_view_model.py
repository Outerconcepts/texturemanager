import operator

from PySide2 import QtCore


class TableViewModel(QtCore.QAbstractTableModel):

    def __init__(self, application, parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent)

        self.application = application

    def rowCount(self, parent=None):
        return self.application.model_info().row_count

    def columnCount(self, parent=None):
        return self.application.model_info().column_count

    def data(self, index, role):
        cell = self.application.cell(index.row(), index.column())

        if role == QtCore.Qt.UserRole:
            return cell

        if role == QtCore.Qt.ToolTipRole and index.column() > 2:
            return cell.long_name

    def setData(self, index, value, role):
        self.application.update_cell(index.row(), index.column(), value)

        return False

    def flags(self, index):
        if index.column() > 2:
            return QtCore.Qt.NoItemFlags

        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable

    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.application.model_info().header_labels[section]

        return None

    # def sort(self, column, order):
    #     self.emit(QtCore.SIGNAL("layoutAboutToBeChanged()"))
    #     self.arraydata = sorted(self.application.table_model.data, key=operator.itemgetter(column))
    #     if order == QtCore.Qt.DescendingOrder:
    #         self.arraydata.reverse()
    #     self.emit(QtCore.SIGNAL("layoutChanged()"))
