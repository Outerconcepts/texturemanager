import json

class dataManager:

    def __init__(self):
        self.folder_path = 'D:/PROJECTS/'
        self.column_titles = ['OBJET', 'UDIM', 'VERSION', 'DIFF', 'SPEC', 'ROUGH', 'NML', 'HEIGHT', 'SSS']
        self.backup_file = 'texture_assign.json'
        self.elements = []
        self.udims = {}

        self.get_elements()
        self.get_udims()

    def set_folder_path(self, path):
        """ Sets the default folder path depending on the passed path.

        :param path: New project path to set as default
        :type path: str
        """
        self.folder_path = path
        print(self.folder_path)

    def get_elements(self):
        """ Gets the existing elements to address inside the texture_assign.json file if it exists. Each element found
        is then stored inside the elements list"""
        with open(self.backup_file, 'r') as data_file:
            data = json.load(data_file)
            for element in data:
                self.elements.append(element)

    def get_udims(self):
        """ Gets the existing udims for each existing element. The existing versions are presented as a dict
        made as follows : {'elementName': '4', 'element2Name': '1'} where the number is the version's number."""
        with open('texture_assign.json', 'r') as data_file:
            data = json.load(data_file)
            for element in data:
                udims = []
                for udim in data[element]:
                    udims.append(udim)
                self.udims[element] = udims

    def save_data(self, in_data):
        """ Saves the data contained in the array to the .json file for backup

        :param in_data: description of the 2D dimensions array as nested lists
        :type in_data: Tuple (list) """
        out_data = {}

        for row_number in range(0, len(in_data)-1):
            element_cell = in_data[row_number][0]
            udim_cell = in_data[row_number][1]
            version_cell = in_data[row_number][2]


            out_data[element_cell.value] = {}
            out_data[element_cell.value][udim_cell.value] = {}
            out_data[element_cell.value][udim_cell.value][version_cell.value] = {}

            for column_number in range(3, len(self.column_titles)):
                texture_type = self.column_titles[column_number]
                texture_path_cell = in_data[row_number][column_number]

                out_data[element_cell.value][udim_cell.value][version_cell.value][texture_type] = texture_path_cell.value

        with open(self.backup_file, 'w') as data_file:
            json.dump(out_data, data_file)
