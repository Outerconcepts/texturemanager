from data import dataManager
import os

class Cell:
    def __init__(self, row, column, value=None):
        self.row = row
        self.column = column
        self.editable = self.is_editable()
        self.value = value
        self.element = ''
        self.texture_type = 'TESTTEXT'
        self.udim = 1
        self.version = 1
        self.short_name = ''
        self.long_name = ''
        self.folder_path = dataManager().folder_path

        if self.column > 2 and self.row < 0:
            self.set_value()


    def __repr__(self):
        return "<Cell('{}')>".format(self.value)

    def set_value(self):
        """ Sets the value of the cell by generating an instance of Path class, and passing it the cell's info"""
        path = Path(element=self.element,
                    texture_type=self.texture_type,
                    version=self.version,
                    udim=self.udim,
                    folder=self.folder_path)

        self.value = path.short_path
        self.short_name = path.short_path
        self.long_name = path.long_path
        return

    def is_editable(self):
        """ Tests the position of the cell in the array to determine if it needs to be editable or not.

        :return: Bool indicating the state of the cell : True if editable, False if not.
        :rtype: Bool
        """
        if self.row > 0 or self.column < 2:
            return True
        else:
            return False


class Path:
    def __init__(self, element, texture_type, version, udim, folder):
        self.element = element
        self.texture_type = texture_type
        self.version = version
        self.udim = udim
        self.short_path = '{}_{}_{}_v{}.png'
        self.long_path = ''
        self.folder_path = folder

        self.set_version()
        self.set_udim()
        self.set_value()

    def set_value(self):
        """ Sets the string value of the current Path instance, depending on its attributes"""
        self.short_path = self.short_path.format(self.element, self.texture_type, self.udim, self.version)

        self.long_path = os.path.join(self.folder_path, self.element, self.short_path)
        self.long_path = os.path.normpath(self.long_path)

    def set_version(self):
        """ Sets the current version of the texture path"""
        version = str(self.version)
        while len(version) < 3:
            version = '0' + version

        self.version = version


    def set_udim(self):
        """ Sets the current udim of the texture path"""
        if len(str(self.udim)) == 4:
            return
        else:
            udim = str(self.udim)
            while len(udim) < 3:
                udim = '0' + udim
            self.udim = '1' + udim
