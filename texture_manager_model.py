from cell import Cell

class TextureManagerModelInfo:

    def __init__(self):
        self.row_count = 0
        self.column_count = 0
        self.header_labels = []
        self.folder_path = ''

class TextureManagerModel:

    def __init__(self, data):
        self.column_titles = data.column_titles
        self.elements = data.elements
        self.udims = data.udims
        self.rows_count = self.set_size()
        self.data = []
        self.folder_path = data.folder_path

        self.build()

    def build(self):
        for row in range(self.rows_count):
            self.data.append([])

            if row >= len(self.elements):
                for column in range(len(self.column_titles)):
                    self.data[row].append(Cell(row=row, column=column))

            else:
                for column in range(len(self.column_titles)):
                    if column == 0:
                        self.data[row].append(Cell(row=row, column=column, value=self.elements[row -1]))

                    if column == 1:
                        element = self.elements[row - 1]
                        self.data[row].append(Cell(row=row, column=column, value=self.udims[element][0]))

                    if column == 2:
                        self.data[row].append(Cell(row=row, column=column, value='1'))

                    if column > 2:
                        self.data[row].append(Cell(row=row, column=column))
                        self.get_cell_info(row, column)

    def update_row(self, row):
        """ Updates all user-non-editable cells inside the given row, to avoid rebuilding the whole array. """
        for cell in self.data[row]:
            if cell.column > 2:
                self.get_cell_info(row, cell.column)

    def info(self):
        """ Generates an instance of TableModelInfo class, then sets the attributes of the instance to match
        requirements, and return the instance generated.

        :return: a set of info concerning the table model
        :rtype instance of class TableModelInfo"""

        info = TextureManagerModelInfo()
        info.row_count = self.rows_count
        info.column_count = len(self.column_titles)
        info.header_labels = self.column_titles
        info.folder_path = self.folder_path

        return info

    def set_size(self):
        """ Sets the array size depending on the elements found by the data manager (if there is any)"""
        if self.elements:
            size = len(self.elements) + 1

            return size
        return 2

    def get_cell_info(self, row, col):
        """ Gets the object, texture type, version and udim assigned to the current cell by looking at column 0,1 and 2
        matching the current cell's row."""
        element_cell = self.data[row][0]
        udim_cell = self.data[row][1]
        version_cell = self.data[row][2]

        cell = self.data[row][col]

        if element_cell.value:
            cell.element = element_cell.value
        else:
            pass

        if udim_cell.value:
            cell.udim = udim_cell.value
        else:
            pass

        if version_cell.value:
            cell.version = version_cell.value
        else:
            pass

        cell.texture_type = self.column_titles[col]
        cell.folder_path = self.folder_path
        cell.set_value()

    def update_cell(self, row, column, value=None):
        """ Updates the cell corresponding to the given row and column by passing it the given value if there is one.

        :param row: number of the row of the cell to update
        :type row: int
        :param column: number of the column of the cell to update
        :type column: int
        :param value: value to pass to the cell
        :type value: str
        """
        cell = Cell(row=row, column=column, value=value)
        self.get_cell_info(row, column)
        self.data[row][column] = cell
        return True


